module github.com/kamalshkeir/korm

go 1.19

require (
	github.com/kamalshkeir/aes v1.0.0
	github.com/kamalshkeir/kinput v0.1.0
	github.com/kamalshkeir/klog v1.0.0
	github.com/kamalshkeir/kmap v1.1.1
	github.com/kamalshkeir/kmux v1.9.4
	github.com/kamalshkeir/ksbus v1.1.2
	github.com/kamalshkeir/kstrct v1.5.5
)

require (
	github.com/kamalshkeir/argon v1.0.0
	golang.org/x/crypto v0.6.0 // indirect
	golang.org/x/net v0.7.0 // indirect
	golang.org/x/sys v0.5.0 // indirect
	golang.org/x/term v0.5.0 // indirect
	golang.org/x/text v0.7.0 // indirect
	golang.org/x/time v0.3.0 // indirect
)
