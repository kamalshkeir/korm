package korm

import (
	"context"
	"net/http"

	"github.com/kamalshkeir/kmux"
)

func init() {
	kmux.BeforeRenderHtml("korm-user", func(reqCtx context.Context, data *map[string]any) {
		const key kmux.ContextKey = "user"
		user, ok := reqCtx.Value(key).(User)
		if ok {
			(*data)["IsAuthenticated"] = true
			(*data)["User"] = user
		} else {
			(*data)["IsAuthenticated"] = false
			(*data)["User"] = nil
		}
	})
}

func initAdminUrlPatterns(r *kmux.Router) {
	media_root := http.FileServer(http.Dir("./" + MediaDir))
	r.GET(`/`+MediaDir+`/*path`, func(c *kmux.Context) {
		http.StripPrefix("/"+MediaDir+"/", media_root).ServeHTTP(c.ResponseWriter, c.Request)
	})
	r.GET("/mon/ping", func(c *kmux.Context) { c.Status(200).Text("pong") })
	r.GET("/offline", OfflineView)
	r.GET("/manifest.webmanifest", ManifestView)
	r.GET("/sw.js", ServiceWorkerView)
	r.GET("/robots.txt", RobotsTxtView)
	adminGroup := r.Group(AdminPathNameGroup)
	adminGroup.GET("/", Admin(IndexView))
	adminGroup.GET("/login", Auth(LoginView))
	adminGroup.POST("/login", Auth(LoginPOSTView))
	adminGroup.GET("/logout", LogoutView)
	adminGroup.POST("/delete/row", Admin(DeleteRowPost))
	adminGroup.POST("/update/row", Admin(UpdateRowPost))
	adminGroup.POST("/create/row", Admin(CreateModelView))
	adminGroup.POST("/drop/table", Admin(DropTablePost))
	adminGroup.GET("/table/:model", Admin(AllModelsGet))
	adminGroup.POST("/table/:model/search", Admin(AllModelsSearch))
	adminGroup.GET("/get/:model/:id", Admin(SingleModelGet))
	adminGroup.GET("/export/:table", Admin(ExportView))
	adminGroup.POST("/import", Admin(ImportView))
}
